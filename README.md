# Boilerplate: PHP with Composer on Docker

This boilerplate is a PHP setup on Docker, that uses Composer for dependency management. It also uses XDebug to debug your application with VSCode. The web server used is Apache.

## Launching

Make sure that both docker and docker-compose are on your PATH. When you're on Windows, make sure Docker uses Linux containers.

### Windows

Run `launch-dev.bat` to install composer dependencies (including development), build the docker container and start it. You can then start the "Listen for XDebug" configuration in VSCode to start debugging.

### Linux / OSX

Same as above, except you should run `launch-dev.sh`.

## Recommendations

I recommend you to change the container name and the image name in `docker-compose.yml`.

## Versions

| Application | Version |
|-------------|---------|
| PHP | 7.2 |
| Composer | Latest |

## Credits

I used the following sources during the development of this boilerplate:

- <https://medium.com/@jasonterando/debugging-with-visual-studio-code-xdebug-and-docker-on-windows-b63a10b0dec>
- <https://www.shiphp.com/blog/2017/composer-php-docker>