#!/bin/bash
# RUN THIS SCRIPT FROM THE REPOSITORY ROOT
docker run --rm -v $(pwd)/src:/app composer:latest install
docker-compose up
docker-compose down