@ECHO OFF
REM RUN THIS SCRIPT FROM THE REPOSITORY ROOT.
docker run --rm -v %cd%/src:/app composer:latest install
docker-compose up
docker-compose down